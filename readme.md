# Smashing Magazine Wallpapers Downloader

Simple script to download all wallpapers from last Smashing Magazine monthly wallpaper post.

## Setup

Just download repository and set a target path for wallpapers in `config.yml`. Then run script from terminal, i.e.:
```
ruby wallpapers.rb
```