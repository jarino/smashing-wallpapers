require 'nokogiri'
require 'open-uri'
require 'yaml'
require 'fileutils'

def backup_old_wallpapers wallpapers_folder_path
	backup_directory_name = File.join(wallpapers_folder_path, 'old')

	FileUtils.mkdir(backup_directory_name) unless File.directory?(backup_directory_name)

	Dir.entries(wallpapers_folder_path).each do |file|
		puts "Backuping #{file}"
		FileUtils.mv(File.join(wallpapers_folder_path, file) , File.join(backup_directory_name)) if !File.directory?(File.join(wallpapers_folder_path,file))
	end	
end

def download_wallpapers wallpapers_folder_path
	doc = Nokogiri::HTML(open('https://www.smashingmagazine.com/tag/wallpapers/'))

	article_url = doc.at_css('article a').attributes['href'].value

	wallpapers_page = Nokogiri::HTML(open(article_url))

	uls = wallpapers_page.css('figure+ul')

	uls.each do |list|
		anchor_to_image = list.search('li')[1].search('>a:last')[0]
		
		image_title = anchor_to_image.attributes['title'].value

		puts "Downloading #{image_title}"

		open(anchor_to_image.attributes['href'].value) do |f|
			File.open(File.join(wallpapers_folder_path, image_title), 'wb') do |file|
				file.puts f.read
			end
		end
	end	
end

config = YAML.load_file('config.yml')

wallpapers_folder = config['path_to_wallpaper_folder']

backup_old_wallpapers wallpapers_folder

download_wallpapers wallpapers_folder